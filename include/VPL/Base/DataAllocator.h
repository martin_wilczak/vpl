//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 *
 * Medical Data Segmentation Toolkit (MDSTk)    \n
 * Copyright (c) 2011 by Michal Spanel          \n
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2011/05/29                          \n
 *
 * Description:
 * - Data allocator policy.
 */

#ifndef VPL_DataAllocator_H
#define VPL_DataAllocator_H

#include <VPL/System/Memory.h>

#include "Assert.h"
#include "Types.h"
#include "Exception.h"
#include "Meta.h"
#include "EmptyConst.h"


namespace vpl
{
namespace base
{

//==============================================================================
/*!
 * Base class for all classes providing data allocation functionality.
 * - Parameter T is an item type.
 */
template <typename T>
struct CDataAllocator
{
    //! Data type.
    typedef T tData;

    //! Pointer to the data.
    typedef T *tDataPtr;

    //! Const data type.
    typedef const T tConstData;

    //! Pointer to the constant data.
    typedef const T *tConstDataPtr;
};


} // namespace base
} // namespace vpl

#endif // VPL_DataAllocator_H

