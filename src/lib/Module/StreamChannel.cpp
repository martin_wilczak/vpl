//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2008 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2008/09/28                       
 *
 * Description:
 * - Buffered channel providing STL-like stream functionality.
 */

#include <VPL/Module/StreamChannel.h>

#include <VPL/System/Memory.h>
#include <VPL/System/SystemTypes.h>
#include <VPL/Math/Base.h>

// STL
#include <istream>
#include <ostream>


namespace vpl
{
namespace mod
{

//==============================================================================
/*
 * Implementation of the class COStreamChannel.
 */
COStreamChannel::COStreamChannel(CChannel *pChannel)
    : m_spChannel(pChannel)
{
    VPL_ASSERT(pChannel);
    
    // Create the output stream
    m_pStream = new std::ostream(this);

    // Buffer initialization
    m_pcBufferEnd = m_pcBuffer + SIZE;

    // Sets the put area
    setp(m_pcBuffer, m_pcBufferEnd);
}


COStreamChannel::~COStreamChannel()
{
    // Flush the buffer
    overflow();

    // Flush the channel
    m_spChannel->flush();
}


int COStreamChannel::overflow(int c)
{
    // Get the data size
    int iSize = int(pptr() - pbase());

    // Write it to the output channels
    if( !m_spChannel->write(pbase(), iSize) )
    {
        m_pStream->setstate(std::ios_base::badbit);
    }

    // Reinitiate the internal buffer
    setp(m_pcBuffer, m_pcBufferEnd);
    if( c != EOF )
    {
        sputc(char(c));
    }

    return 0;
}


int COStreamChannel::underflow()
{
    return 0;
}


int COStreamChannel::sync()
{
    overflow();

    return 0;
}


//==============================================================================
/*
 * Implementation of the class CIStreamChannel.
 */
CIStreamChannel::CIStreamChannel(CChannel *pChannel)
    : m_spChannel(pChannel)
{
    VPL_ASSERT(pChannel);

    // Create the input stream
    m_pStream = new std::istream(this);

    // Buffer initialization
    m_pcBufferEnd = m_pcBuffer + SIZE;

    // Sets the put area
    setg(m_pcBuffer, m_pcBufferEnd, m_pcBufferEnd);
}


CIStreamChannel::~CIStreamChannel()
{
    // Flush the buffer
    underflow();

    // Flush the channel
    m_spChannel->flush();
}


int CIStreamChannel::overflow(int VPL_UNUSED(c))
{
    return 0;
}


int CIStreamChannel::underflow()
{
    // Get the data size
    int iSize = int(egptr() - gptr());
    if( iSize > 0 )
    {
        return 0;
    }

    // Read data from the intput channels
    if( m_spChannel->read(m_pcBuffer, SIZE) <= 0 )
    {
        m_pStream->setstate(std::ios_base::badbit);
    }

    // Reinitiate the internal buffer
    setg(m_pcBuffer, m_pcBuffer, m_pcBufferEnd);

    return 0;
}


int CIStreamChannel::sync()
{
    underflow();

    return 0;
}


} // namespace mod
} // namespace vpl

