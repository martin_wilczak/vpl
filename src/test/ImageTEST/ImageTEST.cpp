//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2004/06/01                       
 *
 * Description:
 * - Testing of the vpl::CImage template
 */

#include <VPL/Base/Logging.h>
#include <VPL/Image/Image.h>
#include <VPL/Image/ImageFunctions.h>

#include "../TestFuncs.h"

// STL
#include <iostream>
#include <ctime>


//==============================================================================
/*!
 * Prints a given image
 */
void printImage(const vpl::img::CDImage& Image, bool bPrintMargin = false)
{
    std::cout.setf(std::ios_base::fixed);
    vpl::tSize Margin = (bPrintMargin) ? Image.getMargin() : 0;
    for( vpl::tSize j = -Margin; j < Image.getYSize() + Margin; j++ )
    {
        std::cout << "  ";
        for( vpl::tSize i = -Margin; i < Image.getXSize() + Margin; i++ )
        {
            std::cout << Image(i, j) << " ";
        }
        std::cout << std::endl;
    }
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
//    VPL_LOG_INIT_FILE("test.log");
    
    vpl::img::CDImagePtr spIm1(new vpl::img::CDImage(10, 10, 1));
    spIm1->fillEntire(vpl::img::CDImage::tPixel(2));
    std::cout << "Smart pointer to image 1 (10x10, margin 1)" << std::endl;
    printImage(*spIm1);
    vpl::test::keypress();

    vpl::img::CDImage Im2(*spIm1, 5, 5, 3, 3, vpl::REFERENCE);
    std::cout << "Creating image 2 using constructor parameters (*spIm1, 5, 5, 3, 3, vpl::REFERENCE)" << std::endl;
    std::cout << " - Reference to the image 1" << std::endl;
    vpl::tSize i, j;
    for( j = 0; j < Im2.getYSize(); j++ )
    {
        for( i = 0; i < Im2.getXSize(); i++ )
        {
            Im2(i,j) = j * Im2.getYSize() + i;
        }
    }
    printImage(Im2);
    vpl::test::keypress();

    std::cout << "Image 1" << std::endl;
    printImage(*spIm1);
    vpl::test::keypress();

    std::cout << "Operation *spIm1 += Im2" << std::endl;
    *spIm1 += Im2;
    printImage(*spIm1);
    vpl::test::keypress();

    std::cout << "Operation Im2 *= 2" << std::endl;
    Im2 *= vpl::CScalar<int>(2);
    printImage(Im2);
    vpl::test::keypress();

    std::cout << "Image 1" << std::endl;
    printImage(*spIm1);
    vpl::test::keypress();

    std::cout << "Operation vpl::img::getMin<double>(*spIm1), getMax(), getSum(), getMean()" << std::endl;
    std::cout << "  " << vpl::img::getMin<double>(*spIm1)
    << "  " << vpl::img::getMax<double>(*spIm1)
    << "  " << vpl::img::getSum<double>(*spIm1)
    << "  " << vpl::img::getMean<double>(*spIm1)
    << std::endl;
    vpl::test::keypress();

    std::cout << "Image 1" << std::endl;
    printImage(*spIm1);
    vpl::test::keypress();

    vpl::img::CDImage Im3(*spIm1, 3, 3, 4, 3);
    std::cout << "Creating image 3 using constructor parameters (*spIm1, 3, 3, 4, 3)" << std::endl;
    std::cout << " - Copy of the image 1" << std::endl;
    printImage(Im3);
    vpl::test::keypress();

    std::cout << "Operation Im3 /= 2" << std::endl;
    Im3 /= vpl::CScalar<int>(2);
    printImage(Im3);
    vpl::test::keypress();

    std::cout << "Image 1" << std::endl;
    printImage(*spIm1);
    vpl::test::keypress();

    Im3.resize(3, 3, 2);
    Im3.fillEntire(vpl::img::CDImage::tPixel(0));
    std::cout << "Creating image 3 with margin (3, 3, tPixel(0), 2)" << std::endl;
    for( j = 0; j < Im3.getYSize(); j++ )
    {
        for( i = 0; i < Im3.getXSize(); i++ )
        {
            Im3(i,j) = i * Im3.getYSize() + j;
        }
    }
    printImage(Im3, true);
    vpl::test::keypress();

    std::cout << "Fill margin of the image 3" << std::endl;
    Im3.fillMargin(1);
    printImage(Im3, true);
    vpl::test::keypress();

    std::cout << "Mirror margin of the image 3" << std::endl;
    Im3.mirrorMargin();
    printImage(Im3, true);
    vpl::test::keypress();

    std::cout << "Image 2" << std::endl;
    printImage(Im2);
    vpl::test::keypress();

    std::cout << "Iterate image 2" << std::endl;
    std::cout << "  ";
    {
        vpl::img::CDImage::tIterator it(Im2);
        std::cout << "it.getSize(): " << it.getSize() << std::endl;
        for( ; it; ++it )
        {
            std::cout << *it << "  ";
        }
        std::cout << std::endl;
        vpl::test::keypress();
    }

    const vpl::img::CDImage Im4(Im2, vpl::REFERENCE);
    std::cout << "Creating const image 4 using constructor parameters (Im2, vpl::REFERENCE)" << std::endl;
    std::cout << " - Reference to the image 2" << std::endl;
    printImage(Im4);
    vpl::test::keypress();

    std::cout << "Iterate image 4" << std::endl;
    std::cout << "- Const iterator" << std::endl;
    std::cout << "  ";
    {
        vpl::img::CDImage::tConstIterator it(Im4);
        std::cout << "it.getSize(): " << it.getSize() << std::endl;
        for( ; it; ++it )
        {
            std::cout << *it << "  ";
        }
        std::cout << std::endl;
        vpl::test::keypress();
    }


    std::cout << "Testing image pixel access overload" << std::endl;
    Im3.resize(1024, 1024, 8);

    int c, COUNT = 100;

    std::cout << "  Basic version" << std::endl;
    vpl::test::tClockCounter Start = vpl::test::start();
    for( c = 0; c < COUNT; ++c )
    {
    	vpl::img::CDImage::tPixel px = vpl::img::CDImage::tPixel(c);
        for( j = 0; j < Im3.getYSize(); ++j )
        {
            for( i = 0; i < Im3.getXSize(); ++i )
            {
                Im3(i, j) = px;
            }
        }
    }
    vpl::test::stop(Start);
    vpl::test::keypress();

    std::cout << "  Iterator version" << std::endl;
    Start = vpl::test::start();
    for( c = 0; c < COUNT; ++c )
    {
    	vpl::img::CDImage::tPixel px = vpl::img::CDImage::tPixel(c);
        for( vpl::img::CDImage::tIterator it(Im3); it; ++it )
        {
            *it = px;
        }
    }
    vpl::test::stop(Start);
    vpl::test::keypress();

    std::cout << "  Fill version" << std::endl;
    Start = vpl::test::start();
    for( c = 0; c < COUNT; ++c )
    {
        Im3.fill(vpl::img::CDImage::tPixel(c));
    }
    vpl::test::stop(Start);
    vpl::test::keypress();

    std::cout << "  Fill entire version" << std::endl;
    Start = vpl::test::start();
    for( c = 0; c < COUNT; ++c )
    {
        Im3.fillEntire(vpl::img::CDImage::tPixel(c));
    }
    vpl::test::stop(Start);

    return 0;
}
