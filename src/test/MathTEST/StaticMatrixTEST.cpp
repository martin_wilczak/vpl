//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2003/12/04                       
 * 
 * Description:
 * - Testing of the vpl::CStaticMatrix template.
 */

#include <VPL/Base/Setup.h>
#include <VPL/Math/StaticMatrix.h>
#include <VPL/Math/StaticVector.h>
#include <VPL/Math/MatrixFunctions.h>

// STL
#include <iostream>
#include <ctime>


//==============================================================================
/*!
 * Prints a given matrix
 */
void printMatrix(vpl::math::CDMatrix3x3& m)
{
    std::cout.setf(std::ios_base::fixed);
    for( vpl::tSize i = 0; i < m.getNumOfRows(); ++i )
    {
        std::cout << "  ";
        for( vpl::tSize j = 0; j < m.getNumOfCols(); ++j )
        {
            std::cout << m(i,j) << " ";
        }
        std::cout << std::endl;
    }
}


//==============================================================================
/*!
 * Prints a given vector
 */
void printVector(vpl::math::CDVector3& v)
{
    std::cout.setf(std::ios_base::fixed);
    std::cout << "  ";
    for( vpl::tSize i = 0; i < v.getSize(); i++ )
    {
        std::cout << v(i) << " ";
    }
    std::cout << std::endl;
}


//==============================================================================
/*!
 * Waiting for a key
 */
void keypress()
{
    while( std::cin.get() != '\n' );
}


//==============================================================================
/*!
 * Clock counter
 */
clock_t ClockCounter;


//==============================================================================
/*!
 * Starts time measuring
 */
void begin()
{
    ClockCounter = clock();
}

//==============================================================================
/*!
 * Stops time measuring and prints result
 */
void end()
{
    ClockCounter = clock() - ClockCounter;
    std::cout << "  Measured clock ticks: " << ClockCounter << std::endl;
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    vpl::math::CDMatrix3x3 m1;
    std::cout << "Unit matrix 1" << std::endl;
    m1.unit();
    printMatrix(m1);
    keypress();

    vpl::math::CDMatrix3x3 m2;
    std::cout << "Matrix 2" << std::endl;
    vpl::tSize i, j;
    for( i = 0; i < m2.getNumOfRows(); i++ )
    {
        for( j = 0; j < m2.getNumOfCols(); j++ )
        {
            m2(i,j) = i * m1.getNumOfCols() + j;
        }
    }
    printMatrix(m2);
    keypress();

    std::cout << "Operation m1 += m2" << std::endl;
    m1 += m2;
    printMatrix(m1);
    keypress();

    std::cout << "Operation m2 *= 2" << std::endl;
    m2 *= vpl::CScalar<int>(2);
    printMatrix(m2);
    keypress();

    vpl::math::CDMatrix3x3 m3;
    std::cout << "Operation m3.mult(m1, m2)" << std::endl;
    m3.mult(m1, m2);
    printMatrix(m3);
    keypress();

    std::cout << "Matrix m1" << std::endl;
    for( i = 0; i < m1.getNumOfRows(); i++ )
    {
        for( j = 0; j < m1.getNumOfCols(); j++ )
        {
            m1(i,j) += i * m1.getNumOfCols() + j;
        }
    }
    printMatrix(m1);
    keypress();

    vpl::math::CDVector3 v1;
    std::cout << "Vector 1" << std::endl;
    for( vpl::tSize j = 0; j < v1.getSize(); j++ )
    {
        v1(j) = j;
    }
    printVector(v1);
    keypress();

    vpl::math::CDVector3 v2;
    std::cout << "Operation v2.mult(m1, v1)" << std::endl;
    v2.mult(m1, v1);
    printVector(v2);
    keypress();

    std::cout << "Operation v2.mult(v1, m1)" << std::endl;
    v2.mult(v1, m1);
    printVector(v2);
    keypress();

    std::cout << "Testing matrix access overhead" << std::endl;
    typedef vpl::math::CStaticMatrix<double,16,16> tTestMatrix;
    tTestMatrix m4;

    std::cout << "  Basic version" << std::endl;
    begin();
    int c, COUNT = 1000000;
    for( c = 0; c < COUNT; ++c )
    {
        for( j = 0; j < m4.getNumOfRows(); ++j )
        {
            for( i = 0; i < m4.getNumOfCols(); ++i )
            {
                m4(i, j) = 10.0;
            }
        }
    }
    end();
//    std::cout << m4;
    keypress();

    // Iterator version
    std::cout << "  Iterator version" << std::endl;
    begin();
    for( c = 0; c < COUNT; ++c )
    {
        for( tTestMatrix::tIterator it(m4); it; ++it )
        {
            *it = 11.0;
        }
    }
    end();
//    std::cout << m4;
    keypress();

    // Fill version
    std::cout << "  Fill version" << std::endl;
    begin();
    for( c = 0; c < COUNT; ++c )
    {
        m4.fill(12.0);
    }
    end();
//    std::cout << m4;

    return 0;
}

