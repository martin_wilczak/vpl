//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2006/06/27                       
 * 
 * Description:
 * - Testing of the vpl::math::CComplex template.
 */

#include <VPL/Math/Complex.h>
#include <VPL/Math/Vector.h>
#include <VPL/Math/VectorFunctions.h>

// STL
#include <iostream>


//==============================================================================
/*
 * Global definitions.
 */

//! Vector of complex numbers.
typedef vpl::math::CVector<vpl::math::CDComplex> tDComplexVector;


//==============================================================================
/*!
 * Prints a given vector
 */
void printVector(tDComplexVector& v)
{
    std::cout.setf(std::ios_base::fixed);
    std::cout << "  ";
    for( vpl::tSize i = 0; i < v.getSize(); i++ )
    {
        std::cout << v(i) << " ";
    }
    std::cout << std::endl;
}


//==============================================================================
/*!
 * Waiting for a key
 */
void keypress()
{
    while( std::cin.get() != '\n' );
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    vpl::math::CDComplex c1(4.0, 3.0);
    std::cout << "Number c1(4.0, 3.0): " << c1 << std::endl;
    std::cout << "Required result: (4,3)" << std::endl;
    keypress();

    vpl::math::CDComplex c2(vpl::math::polar(5.0, 0.75));
    std::cout << "Number c2(vpl::math::polar(5.0, 0.75)): " << c2 << std::endl;
    std::cout << "Required result: (3.65844,3.40819)" << std::endl;
    keypress();

    std::cout << "Operation vpl::math::getAbs(c1): " << vpl::math::getAbs(c1) << std::endl;
    std::cout << "Required result: 5" << std::endl;
    keypress();

    std::cout << "Operation c1.getNorm(): " << c1.getNorm() << std::endl;
    std::cout << "Required result: 25" << std::endl;
    keypress();

    std::cout << "Operation c1.getArg(): " << c1.getArg() << std::endl;
    std::cout << "Required result: 0.643501" << std::endl;
    keypress();

    std::cout << "Operation c1.getConj(): " << c1.getConj() << std::endl;
    std::cout << "Required result: (4,-3)" << std::endl;
    keypress();

    std::cout << "Operation 4.4 + c1 * 1.8: " << 4.4 + c1 * 1.8 << std::endl;
    std::cout << "Required result: (11.6,5.4)" << std::endl;
    keypress();

    std::cout << "Operation c1 + c2: " << c1 + c2 << std::endl;
    std::cout << "Required result: (7.65844,6.40819)" << std::endl;
    keypress();

/*  std::cout << "Operation c1 += vpl::math::getSqrt(c1): " << (c1 += vpl::math::getSqrt(c1)) << std::endl;
    std::cout << "Required result: (6.12132,3.70711)" << std::endl;
    keypress();*/

    tDComplexVector v1(4);
    v1.fill(vpl::math::CDComplex(1, 0.5));
    std::cout << "Vector v1(4, vpl::math::CDComplex(1, 0.5))" << std::endl;
    printVector(v1);
    keypress();

    tDComplexVector v2(4);
    std::cout << "Vector v2(4)" << std::endl;
    for( vpl::tSize j = 0; j < v2.getSize(); j++ )
    {
        v2(j) = j;
    }
    printVector(v2);
    keypress();

    std::cout << "Operation v1 += v2" << std::endl;
    v1 += v2;
    printVector(v1);
    keypress();

    tDComplexVector::tSmartPtr spV3(new tDComplexVector(8));
    spV3->fill(vpl::math::CDComplex(0, 0));
    std::cout << "New smart pointer to vector 3" << std::endl;
    printVector(*spV3);
    keypress();

    std::cout << "Fill the spV3 by the value (1, 3)" << std::endl;
    spV3->fill(vpl::math::CDComplex(1, 3));
    printVector(*spV3);
    keypress();

/*    tDComplexVector v31(*spV3, 0, 4, vpl::REFERENCE);
    std::cout << "Reference v31" << std::endl;
    printVector(v31);
    keypress();

    tDComplexVector v32(*spV3, 4, 4, vpl::REFERENCE);
    std::cout << "Reference v32" << std::endl;
    printVector(v32);
    keypress();

    std::cout << "Operation v31 = v2, v32 = v1" << std::endl;
    v31 += v2; v32 += v1;
    printVector(*spV3);
    keypress();

    std::cout << "Operation v31 *= vpl::math::CDComplex(2, 0), v32 += vpl::math::CDComplex(10, 0)" << std::endl;
    v31 *= vpl::math::CDComplex(2, 0);
    v32 += vpl::math::CDComplex(10, 0);
    printVector(*spV3);
    keypress();*/

    std::cout << "Operation getSum(), getMean()" << std::endl;
    std::cout << "  " << vpl::math::getSum<vpl::math::CDComplex>(*spV3)
        << "  " << vpl::math::getMean<vpl::math::CDComplex>(*spV3)
        << std::endl;
    keypress();

    std::cout << "Operation v1.subSample(*spV3, 2)" << std::endl;
    v1.subSample(*spV3, 2);
    printVector(v1);
    keypress();

    std::cout << "Vector 2" << std::endl;
    printVector(v2);
    keypress();

    std::cout << "Operation spV3->concat(v1, v2)" << std::endl;
    spV3->concat(v1, v2);
    printVector(*spV3);
    keypress();

/*    std::cout << "Operation v31.resize(16).fill(vpl::math::CDComplex(5, 0))" << std::endl;
    v31.resize(16).fill(vpl::math::CDComplex(5, 0));
    printVector(v31);
    keypress();

    std::cout << "Vector 3" << std::endl;
    printVector(*spV3);
    keypress();

    std::cout << "Vector 31" << std::endl;
    printVector(v31);
    keypress();

    std::cout << "Vector 32" << std::endl;
    printVector(v32);
    keypress();*/

    return 0;
}

