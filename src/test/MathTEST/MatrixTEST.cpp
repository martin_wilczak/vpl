//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2003/12/04                       
 * 
 * Description:
 * - Testing of the vpl::CMatrix template.
 */

#include <VPL/Base/Setup.h>
#include <VPL/Math/Matrix.h>
#include <VPL/Math/Vector.h>
#include <VPL/Math/MatrixFunctions.h>

// STL
#include <iostream>
#include <ctime>


//==============================================================================
/*!
 * Prints a given matrix
 */
void printMatrix(vpl::math::CDMatrix& m)
{
    std::cout.setf(std::ios_base::fixed);
    for( vpl::tSize i = 0; i < m.getNumOfRows(); i++ )
    {
        std::cout << "  ";
        for( vpl::tSize j = 0; j < m.getNumOfCols(); j++ )
        {
            std::cout << m(i,j) << " ";
        }
        std::cout << std::endl;
    }
}


//==============================================================================
/*!
 * Prints a given vector
 */
void printVector(vpl::math::CDVector& v)
{
    std::cout.setf(std::ios_base::fixed);
    std::cout << "  ";
    for( vpl::tSize i = 0; i < v.getSize(); i++ )
    {
        std::cout << v(i) << " ";
    }
    std::cout << std::endl;
}


//==============================================================================
/*!
 * Waiting for a key
 */
void keypress()
{
    while( std::cin.get() != '\n' );
}


//==============================================================================
/*!
 * Clock counter
 */
clock_t ClockCounter;


//==============================================================================
/*!
 * Starts time measuring
 */
void begin()
{
    ClockCounter = clock();
}

//==============================================================================
/*!
 * Stops time measuring and prints result
 */
void end()
{
    ClockCounter = clock() - ClockCounter;
    std::cout << "  Measured clock ticks: " << ClockCounter << std::endl;
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    vpl::math::CDMatrix m1(4, 4);
    m1.unit();
    std::cout << "Unit matrix 1" << std::endl;
    printMatrix(m1);
    keypress();

    vpl::math::CDMatrix m2(4, 4);
    std::cout << "Matrix 2" << std::endl;
    vpl::tSize i, j;
    for( i = 0; i < m2.getNumOfRows(); i++ )
    {
        for( j = 0; j < m2.getNumOfCols(); j++ )
        {
            m2(i,j) = i * m2.getNumOfRows() + j;
        }
    }
    printMatrix(m2);
    keypress();

    std::cout << "Iterate over all matrix elements:" << std::endl;
    for( vpl::math::CDMatrix::tIterator It(m2); It; ++It )
    {
        std::cout << " " << *It;
    }
    std::cout << std::endl;
    keypress();

    std::cout << "Operation m3.asEigen() = m2.asEigen().block(1,1,2,3)" << std::endl;
    vpl::math::CDMatrix m3;
    m3.asEigen() = m2.asEigen().block(1,1,2,3);
    printMatrix(m3);
    keypress();

//    std::cout << "Operation m3 = m2.asEigen().block(1,1,4,4)" << std::endl;
//    m3 = m2.asEigen().block(1,1,4,4);
//    printMatrix(m3);
//    keypress();

    std::cout << "Operation v1.asEigen() = m2.asEigen().block(0,2,m2.getNumOfRows(),1)" << std::endl;
    vpl::math::CDVector v1;
    v1.asEigen() = m2.asEigen().block(0,2,m2.getNumOfRows(),1);
    printVector(v1);
    keypress();

    std::cout << "Operation m1 += m2" << std::endl;
    m1 += m2;
    printMatrix(m1);
    keypress();

    std::cout << "Operation m2 *= 2" << std::endl;
    m2 *= vpl::CScalar<int>(2);
    printMatrix(m2);
    keypress();

    m3.resize(4, 4);
    std::cout << "Operation m3.mult(m1, m2)" << std::endl;
    m3.mult(m1, m2);
    printMatrix(m3);
    keypress();

    std::cout << "Matrix m1" << std::endl;
    for( i = 0; i < m1.getNumOfRows(); i++ )
    {
        for( j = 0; j < m1.getNumOfCols(); j++ )
        {
            m1(i,j) += i * m1.getNumOfCols() + j;
        }
    }
    printMatrix(m1);
    keypress();

    std::cout << "Operation m2.transpose(m1)" << std::endl;
    m2.transpose(m1);
    printMatrix(m2);
    keypress();

    std::cout << "Operation getDeterminant<double>(m2)" << std::endl;
    begin();
    double dDet = vpl::math::getDeterminant<double>(m2);
    std::cout << "  " << dDet << std::endl;
    keypress();

    std::cout << "Operation m1 = m2, inverse(m1)" << std::endl;
    m1 = m2;
    vpl::math::inverse(m1);
    printMatrix(m1);
    keypress();

    std::cout << "Operation m3.mult(m1, m2)" << std::endl;
    m3.mult(m1, m2);
    printMatrix(m3);
    keypress();

    std::cout << "Matrix m1" << std::endl;
    printMatrix(m1);
    keypress();

    vpl::math::CDMatrix m4;

    std::cout << "Re-creating matrix m4 using parameters (m1, 1, 1, 2, 2)" << std::endl;
    m4.copy(m1, 1, 1, 2, 2);
    printMatrix(m4);
    keypress();

    std::cout << "Operation m4 *= 10" << std::endl;
    m4 *= vpl::CScalar<int>(10);
    printMatrix(m4);
    keypress();

    std::cout << "Matrix m1" << std::endl;
    printMatrix(m1);
    keypress();

    vpl::math::CDMatrix m5(5, 5), m55(5, 5);
    std::cout << "Matrix 5" << std::endl;
    m5(0,0) = 2; m5(0,1) = 1; m5(0,2) = 0; m5(0,3) = 0; m5(0,4) = 0;
    m5(1,0) = 1; m5(1,1) = 2; m5(1,2) = 1; m5(1,3) = 0; m5(1,4) = 0;
    m5(2,0) = 0; m5(2,1) = 1; m5(2,2) = 2; m5(2,3) = 1; m5(2,4) = 0;
    m5(3,0) = 0; m5(3,1) = 0; m5(3,2) = 1; m5(3,3) = 2; m5(3,4) = 1;
    m5(4,0) = 0; m5(4,1) = 0; m5(4,2) = 0; m5(4,3) = 1; m5(4,4) = 2;
    printMatrix(m5);
    keypress();


    std::cout << "Operation getDeterminant<double>(m5)" << std::endl;
    int c, COUNT = 100000;
//    begin();
//    for( c = 0; c < COUNT; ++c )
    {
        dDet = vpl::math::getDeterminant<double>(m5);
    }
//    end();
    std::cout << "  " << dDet << std::endl;
    keypress();


    vpl::math::CDVector v(5);
    std::cout << "Operation eig(m5, v)" << std::endl;
//    vpl::math::CDMatrix m5temp(m5);
//    begin();
//    for( c = 0; c < COUNT; ++c )
    {
//        m5 = m5temp;
        vpl::math::eig(m5, v);
    }
//    end();
    std::cout << "Eigenvectors:" << std::endl;
    printMatrix(m5);
    std::cout << "Eigenvalues:" << std::endl;
    printVector(v);
    keypress();


    std::cout << "Testing matrix access overhead" << std::endl;
    typedef vpl::math::CMatrix<double> tTestMatrix;
    tTestMatrix m6(64,64);

    std::cout << "  Basic version" << std::endl;
    begin();
    for( c = 0; c < COUNT; ++c )
    {
        for( j = 0; j < m6.getNumOfRows(); ++j )
        {
            for( i = 0; i < m6.getNumOfCols(); ++i )
            {
                m6(j, i) = 10.0;
            }
        }
    }
    end();
    keypress();

    std::cout << "  Iterator version" << std::endl;
    begin();
    for( c = 0; c < COUNT; ++c )
    {
        for( tTestMatrix::tIterator it(m6); it; ++it )
        {
            *it = 11.0;
        }
    }
    end();
    keypress();

    std::cout << "  Fill version" << std::endl;
    begin();
    for( c = 0; c < COUNT; ++c )
    {
        m6.fill(12.0);
    }
    end();

    return 0;
}

