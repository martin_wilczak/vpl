//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2006 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2006/09/15                       
 *
 * Description:
 * - Testing of the vpl::math::CFuzzyCMeans template.
 */

#include <VPL/Math/Vector.h>
#include <VPL/Math/VectorFunctions.h>

// Enable logging
#define FCM_LOGGING_ENABLED

#include <VPL/Math/Algorithm/FuzzyCMeans.h>

// STL
#include <iostream>


//==============================================================================
/*
 * Global constants.
 */

//! Number of input samples.
const vpl::tSize NUM_OF_SAMPLES = 1000;


//==============================================================================
/*!
 * Prints a given vector.
 */
void printVector(vpl::math::CFVector& v)
{
    std::cout.setf(std::ios_base::fixed);
    std::cout << "  ";
    for( vpl::tSize i = 0; i < v.getSize(); i++ )
    {
        std::cout << v(i) << " ";
    }
    std::cout << std::endl;
}


//==============================================================================
/*!
 * Waiting for a key.
 */
void keypress()
{
    while( std::cin.get() != '\n' );
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    // Init global log
    VPL_LOG_ADD_CERR_APPENDER;
//    VPL_LOG_ADD_FILE_APPENDER("temp.log");

    // One third of all samples
    vpl::tSize THIRD = NUM_OF_SAMPLES / 3;

    std::cout << "Vector 1" << std::endl;
    vpl::math::CFVector v1(NUM_OF_SAMPLES);
    v1.fill(1);
    v1.asEigen().segment(THIRD, THIRD).fill(2);
    v1.asEigen().segment(2 * THIRD, THIRD).fill(3);
    printVector(v1);
    keypress();

    vpl::math::CFuzzyCMeans<vpl::math::CFVector,1> Clustering;
//    if( !Clustering.execute(v1, 3) )
    if( !Clustering.execute(v1) )
    {
        std::cout << "Error: Fuzzy C-Means algorithm failed!" << std::endl;
        return 0;
    }
    
    vpl::math::CFuzzyCMeans<vpl::math::CFVector,1>::tCluster c;
    std::cout << "Number of clusters: " << Clustering.getNumOfClusters() << std::endl;
    for( vpl::tSize i = 0; i < Clustering.getNumOfClusters(); ++i )
    {
        Clustering.getCluster(i, c);
        std::cout << "  Cluster " << i << ": " << c << std::endl;
    }

    vpl::math::CFuzzyCMeans<vpl::math::CFVector,1>::tVector v;
    std::cout << "Membership function:"<< std::endl;
    Clustering.getMembership(150, v);
    std::cout << "  Sample 150: " << v << std::endl;
    Clustering.getMembership(450, v);
    std::cout << "  Sample 450: " << v << std::endl;
    Clustering.getMembership(750, v);
    std::cout << "  Sample 750: " << v << std::endl;

    return 0;
}

