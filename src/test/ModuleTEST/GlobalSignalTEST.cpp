//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2008 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2008/02/28                       
 *
 * Description:
 * - Testing of the vpl::mod::CGlobalSignal class template.
 */

#include <VPL/Module/GlobalSignal.h>

// STL
#include <string>
#include <iostream>


//==============================================================================
/*
 * Definition of global signals.
 */
VPL_DECLARE_SIGNAL_1(0, void, int, SigPrint1);
VPL_DECLARE_SIGNAL_2(1, void, int, double, SigPrint2);


//==============================================================================
/*!
 * Test function.
 */
void print1(int a)
{
    std::cout << "  void print1(" << a << ")" << std::endl;
}


//==============================================================================
/*!
 * Test object.
 */
class CTest
{
public:
    //! Default constructor
    CTest() {}

    //! Destructor
    virtual ~CTest() {}

    //! Method
    virtual void print1(int a)
    {
        std::cout << "  virtual void CTest::print1(" << a << ")" << std::endl;
    }

    //! Method
    static void print2(int a, double b)
    {
        std::cout << "  static void CTest::print2(" << a << ", " << b << ")" << std::endl;
    }
};


//==============================================================================
/*!
 * Waiting for a key
 */
void keypress()
{
    while( std::cin.get() != '\n' );
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    std::cout << "Signal SigPrint1 (id = 0)" << std::endl;
    std::cout << "  Register a new signal handler, print1() function" << std::endl;
    vpl::mod::tSignalConnection Con01 = VPL_SIGNAL(SigPrint1).connect(print1);
    std::cout << "  Invoke the signal" << std::endl;
    VPL_SIGNAL(SigPrint1).invoke(10);
    
    CTest Test;
    
    std::cout << "  Register the CTest::print1() function and invoke" << std::endl;
    vpl::mod::tSignalConnection Con02 = VPL_SIGNAL(SigPrint1).connect(&Test, &CTest::print1);
    VPL_SIGNAL(SigPrint1).invoke(20);

    std::cout << "  Block the print1() function and invoke" << std::endl;
    VPL_SIGNAL(SigPrint1).block(Con01);
    VPL_SIGNAL(SigPrint1).invoke(30);
    
    std::cout << "  Deregister both functions and invoke the signal" << std::endl;
    VPL_SIGNAL(SigPrint1).disconnect(Con02);
    VPL_SIGNAL(SigPrint1).disconnect(Con01);
    VPL_SIGNAL(SigPrint1).invoke(40);
    keypress();

    std::cout << "Signal SigPrint2 (id = 1)" << std::endl;
    std::cout << "  Register a new handler, CTest::print2() function" << std::endl;
    vpl::mod::tSignalConnection Con10 = VPL_SIGNAL(SigPrint2).connect(CTest::print2);
    std::cout << "  Invoke the signal" << std::endl;
    VPL_SIGNAL(SigPrint2).invoke(101, 102);
    keypress();

    return 0;
}
