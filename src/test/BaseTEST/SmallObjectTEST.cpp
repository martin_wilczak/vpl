//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2004/11/22                       
 *
 * Description:
 * - Testing of the vpl::CSmallObject template.
 */

#include <VPL/Base/SmallObject.h>

#include <ctime>

// STL
#include <iostream>


//==============================================================================
/*
 * Global constants and variables.
 */

//! The number of testing loops (object allocation/free)
const int COUNT     = 10000;

//! The number of outer loops
const int COUNT2    = 100;

//! Clock counter
clock_t ClockCounter;


//==============================================================================
/*!
 * Class CSA
 * - uses small object allocation functions
 */
class CSA : public vpl::base::CSmallObject<>
{
public:
    //! Internal data
    double m_dData;

public:
    //! Constructor
    CSA() : m_dData(1.0) {}

    //! Destructor
    virtual ~CSA() {}
};


//==============================================================================
/*!
 * Class CSAA
 * - uses small object allocation functions
 */
class CSAA : public CSA
{
public:
    //! Additional internal data
    double m_dData2;

public:
    //! Constructor
    CSAA() : m_dData2(2.0) {}

    //! Destructor
    virtual ~CSAA() {}
};


//==============================================================================
/*!
 * Class CSV
 * - Uses small object allocation functions
 */
class CSV : public vpl::base::CSmallValueObject<>
{
public:
    //! Internal data
    double m_dData;

public:
    //! Constructor
    CSV() : m_dData(1.0) {}

    //! Destructor
    ~CSV() {}
};


//==============================================================================
/*!
 * Class CA
 * - default behavior new and delete operators
 */
class CA
{
public:
    //! Internal data
    double m_dData;

public:
    //! Constructor
    CA() : m_dData(1.0) {}

    //! Destructor
    virtual ~CA() {}
};


//==============================================================================
/*!
 * Class CAA
 * - default behavior new and delete operators
 */
class CAA : public CA
{
public:
    //! Additional internal data
    double m_dData2;

public:
    //! Constructor
    CAA() : m_dData2(2.0) {}

    //! Destructor
    virtual ~CAA() {}
};


//==============================================================================
/*!
 * Class CV
 * - Default behavior of new and delete operators
 */
class CV
{
public:
    //! Internal data
    double m_dData;

public:
    //! Constructor
    CV() : m_dData(1.0) {}

    //! Destructor
    ~CV() {}
};


//==============================================================================
/*
 * Global functions.
 */

//! Waiting for a key
void keypress()
{
    while( std::cin.get() != '\n' );
}


//! Starts time measuring
void begin()
{
    ClockCounter = clock();
}


//! Stops time measuring and prints results
void end()
{
    ClockCounter = clock() - ClockCounter;
    std::cout << "  Measured clock ticks: " << ClockCounter << std::endl;
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    int i, j;

    CSA *ppSA[COUNT];
    std::cout << "Allocate and free a large number of small objects CSA" << std::endl;
    begin();
    for( j = 0; j < COUNT2; ++j )
    {
        for( i = 0; i < COUNT; ++i )
        {
            ppSA[i] = new CSA;
        }
        for( i = 0; i < COUNT; ++i )
        {
            delete ppSA[i];
        }
    }
    end();
    keypress();

    CA *ppA[COUNT];
    std::cout << "Allocate and free a large number of objects CA" << std::endl;
    begin();
    for( j = 0; j < COUNT2; ++j )
    {
        for( i = 0; i < COUNT; ++i )
        {
            ppA[i] = new CA;
        }
        for( i = 0; i < COUNT; ++i )
        {
            delete ppA[i];
        }
    }
    end();
    keypress();

    std::cout << "Repetitive allocation and deallocation of the small object CSAA" << std::endl;
    begin();
    for( j = 0; j < COUNT2; ++j )
    {
        for( i = 0; i < COUNT; ++i )
        {
            CSAA *p1 = new CSAA;
            CSAA *p2 = new CSAA;
            delete p2;
            delete p1;
        }
    }
    end();
    keypress();

    std::cout << "Repetitive allocation and deallocation of the object CAA" << std::endl;
    begin();
    for( j = 0; j < COUNT2; ++j )
    {
        for( i = 0; i < COUNT; ++i )
        {
            CAA *p1 = new CAA;
            CAA *p2 = new CAA;
            delete p2;
            delete p1;
        }
    }
    end();
    keypress();

    CSV *ppSV[COUNT];
    std::cout << "Allocate and free a large number of small value objects CSV" << std::endl;
    begin();
    for( j = 0; j < COUNT2; ++j )
    {
        for( i = 0; i < COUNT; ++i )
        {
            ppSV[i] = new CSV;
        }
        for( i = 0; i < COUNT; ++i )
        {
            delete ppSV[i];
        }
    }
    end();
    keypress();

    CV *ppV[COUNT];
    std::cout << "Allocate and free a large number of objects CV" << std::endl;
    begin();
    for( j = 0; j < COUNT2; ++j )
    {
        for( i = 0; i < COUNT; ++i )
        {
            ppV[i] = new CV;
        }
        for( i = 0; i < COUNT; ++i )
        {
            delete ppV[i];
        }
    }
    end();
    keypress();

    std::cout << "Repetitive allocation and deallocation of the small value object CSV" << std::endl;
    begin();
    for( j = 0; j < COUNT2; ++j )
    {
        for( i = 0; i < COUNT; ++i )
        {
            CSV *p1 = new CSV;
            CSV *p2 = new CSV;
            delete p2;
            delete p1;
        }
    }
    end();
    keypress();

    std::cout << "Repetitive allocation and deallocation of the object CV" << std::endl;
    begin();
    for( j = 0; j < COUNT2; ++j )
    {
        for( i = 0; i < COUNT; ++i )
        {
            CV *p1 = new CV;
            CV *p2 = new CV;
            delete p2;
            delete p1;
        }
    }
    end();
    keypress();

    return 0;
}

