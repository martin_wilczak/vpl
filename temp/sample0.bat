@echo off 
rem Please, modify and run the 'wsetenv.bat' batch file first in order to
rem setup path to built binaries, etc.

echo Read and show the JPEG image as medical slice...
mdsLoadJPEG <berounka.jpg |mdsSliceRange -auto |mdsSliceView

echo Read and show the JPEG image as RGB image...
mdsLoadJPEG -format rgb <berounka.jpg |mdsRGBImageView

echo Histogram equalization...
mdsLoadJPEG <berounka.jpg |mdsSliceHistEqualization |mdsSliceRange -auto |mdsSliceView

echo Affine transform...
mdsLoadJPEG <berounka.jpg |mdsSliceAffineTransform -alpha 50.0 -sx 2 -sy 2 -fill -interpolator bicubic |mdsSliceRange -auto |mdsSliceView

