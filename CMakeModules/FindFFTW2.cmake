# CMake module that tries to find FFTW includes and libraries.
#
# FFTW can be compiled and subsequently linked against various data types.
#   There is a single set of include files, and then muttiple libraries,
#   One for each type. I.e. libfftw.a->double, libfftwf.a->float
#
# The following variables are set if FFTW is found:
#   FFTWF_FOUND        - True when the FFTW single precision library was found.
#   FFTWD_FOUND        - True when the FFTW double precision library was found.
#   FFTW_INCLUDE_DIR   - The directory where FFTW include files are.
#   FFTWF_LIBRARIES    - List of all single precision FFTW libraries.
#   FFTWD_LIBRARIES    - List of all double precision FFTW libraries.
#
# Usage:
#   In your CMakeLists.txt file do something like this:
#   ...
#   FIND_PACKAGE(FFTW)
#   ...
#   INCLUDE_DIRECTORIES(${FFTW_INCLUDE_DIR})
#   ...
#   TARGET_LINK_LIBRARIES( mytarget ${FFTWF_LIBRARIES} ${FFTWD_LIBRARIES} )

# Predefined search directories
if( WIN32 )
  set( FFTW_INC_SEARCHPATH
       "${FFTW_ROOT_DIR}"
       "$ENV{FFTW_ROOT_DIR}"
       "${INCLUDE_INSTALL_DIR}"
       "$ENV{ProgramFiles}/fftw"
       C:/fftw
       D:/fftw
       )
  set( FFTW_INC_SEARCHSUFFIXES
       include
       )
else( WIN32 )
  set( FFTW_INC_SEARCHPATH
       "${FFTW_ROOT_DIR}"
       "$ENV{FFTW_ROOT_DIR}"
       "${INCLUDE_INSTALL_DIR}"
       /sw/include
       /usr/include
       /usr/local/include
       /opt/include
       /opt/local/include
       )
  set( FFTW_INC_SEARCHSUFFIXES
       fftw
       fftw/include
       )
endif( WIN32 )


find_path( FFTW_INCLUDE_DIR
           NAMES
           fftw3.h
           PATHS
           ${FFTW_INC_SEARCHPATH}
           PATH_SUFFIXES
           ${FFTW_INC_SEARCHSUFFIXES}
           )

get_filename_component( FFTW_INCLUDE_DIR_PATH ${FFTW_INCLUDE_DIR} PATH )

set( FFTW_LIB_SEARCHPATH
     ${FFTW_INCLUDE_DIR_PATH}/lib
     ${LIB_INSTALL_DIR}
     /usr/lib
     /usr/local/lib
     )

MARK_AS_ADVANCED(FFTWD_LIBRARIES)
FIND_LIBRARY(FFTWD_LIB NAMES fftw3 libfftw3 PATHS ${FFTW_LIB_SEARCHPATH}) #Double Precision Lib
FIND_LIBRARY(FFTWD_THREADS_LIB NAMES fftw3_threads libfftw3_threads PATHS ${FFTW_LIB_SEARCHPATH}) #Double Precision Lib only if compiled with threads support

IF( FFTWD_LIBRARIES )
  SET( FFTWD_FOUND 1 )
  IF( FFTWD_THREADS_LIB )
    SET( FFTWD_LIBRARIES ${FFTWD_LIBRARIES} ${FFTWD_THREADS_LIB} )
  ENDIF( FFTWD_THREADS_LIB )
ENDIF( FFTWD_LIBRARIES )

MARK_AS_ADVANCED(FFTWF_LIBRARIES)
FIND_LIBRARY(FFTWF_LIBRARIES NAMES fftw3f libfftw3f PATHS ${FFTW_LIB_SEARCHPATH}) #Single Precision Lib
FIND_LIBRARY(FFTWF_THREADS_LIB NAMES fftw3f_threads libfftw3f_threads PATHS ${FFTW_LIB_SEARCHPATH}) #Single Precision Lib only if compiled with threads support

IF( FFTWF_LIBRARIES )
  SET( FFTWF_FOUND 1 )
  IF( FFTWF_THREADS_LIB )
    SET( FFTWF_LIBRARIES ${FFTWF_LIBRARIES} ${FFTWF_THREADS_LIB} )
  ENDIF( FFTWF_THREADS_LIB )
ENDIF( FFTWF_LIBRARIES )


# display help message
if( NOT FFTWF_FOUND )
  # make FIND_PACKAGE friendly
  if( NOT FFTW_FIND_QUIETLY )
    if( FFTW_FIND_REQUIRED )
      message( FATAL_ERROR "FFTW required but some headers or libs not found. Please specify it's location with FFTW_ROOT_DIR variable.")
      set( FFTW_ROOT_DIR "" CACHE PATH "FFTW root dir" )
    else( FFTW_FIND_REQUIRED )
      message(STATUS "ERROR: FFTW was not found.")
    endif( FFTW_FIND_REQUIRED )
  endif( NOT FFTW_FIND_QUIETLY )
endif( NOT FFTWF_FOUND )
